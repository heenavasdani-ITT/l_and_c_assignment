using System;
using Newtonsoft.Json.Linq;
namespace API {
    ///<summary>Class <c>Blog</c>represents Blog of Tumblr whose details displayed by this class.</summary>
    class Blog : Tumblr {
        dynamic blogDetails;
        /// <summary>method <c>TakeBlogNameAndPostRange</c>is used to take blog name and post range as input from user..</summary>
        public string TakeBlogNameAndPostRange (Tumblr tumblr) {
            string postRange, blogNameQueryString = "Enter the Tumblr blog name", postRangeQueryString = "Enter the minimum and maximum for Post range";
            Console.WriteLine (blogNameQueryString);
            tumblr.blogName = Console.ReadLine ();
            Console.WriteLine (postRangeQueryString);
            postRange = Console.ReadLine ();
            return postRange;
        }
        /// <summary>method <c>DisplayBlogBasicInfo</c>displays title,name,description and number of post for a particular blog.</summary>
        public void DisplayBlogBasicInfo (dynamic blogDetails) {
            string invalidBlogName = "This blog Name is not exist";
            if (blogDetails["posts-total"] != 0) {
                Console.WriteLine ("title=" + blogDetails.tumblelog.title);
                Console.WriteLine ("name=" + blogDetails.tumblelog.name);
                Console.WriteLine ("description=" + blogDetails.tumblelog.description);
                Console.WriteLine ("no of post=" + blogDetails["posts-total"]);
            } else {
                Console.WriteLine (invalidBlogName);
            }
        }
        /// <summary>method <c>DisplayListOfUrlOfImages</c>is used to display all photo urls for the user's specified post range.</summary>
        public void DisplayListOfUrlOfImages (dynamic blogDetails, Blog blog) {
            postNumber = Int32.Parse (blog.startPostNum);
            string imageQuality = "photo-url-1280";
            foreach (var post in blogDetails["posts"]) {
                if (post[imageQuality] != null) {
                    var imageUrl = post[imageQuality];
                    Console.WriteLine (postNumber.ToString () + "." + imageUrl);
                    foreach (var photo in post["photos"]) {
                        if (photo[imageQuality] != null && imageUrl != photo[imageQuality]) {
                            Console.WriteLine ("  " + photo[imageQuality]);
                        }
                    }
                    postNumber++;
                }
            }
            blog.FetchPostsMoreThanApiLimit (blog);
        }
        /// <summary>method <c>FetchPostsMoreThanApiLimit</c>is used to fetch response more than apiLimit.</summary>
        public void FetchPostsMoreThanApiLimit (Blog blog) {
            if ((blog.postNumber - 1) < Int32.Parse (blog.endPostNum)) {
                blog.startPostNum = (postNumber).ToString ();
                blog.GetBlogUrl (blog);
                blog.GetTumblrApiResponse (blog);
                blog.blogDetails = JObject.Parse (blog.validJson);
                blog.DisplayListOfUrlOfImages (blogDetails, blog);
            }
        }
        public static void Main () {
            bool postRangeValidityStatus;
            string invalidInputMsg = "You have entered invalid input";
            var blog = new Blog ();
            blog.postRange = blog.TakeBlogNameAndPostRange (blog);
            blog.GetStartAndEndPostNum (blog);
            postRangeValidityStatus = blog.CheckValidityOfStartAndEndPostNum (blog.startPostNum, blog.endPostNum) && blog.IsBlogNameValid (blog.blogName);
            if (postRangeValidityStatus == false) {
                Console.WriteLine (invalidInputMsg);
            } else {
                blog.GetBlogUrl (blog);
                blog.GetTumblrApiResponse (blog);
                blog.blogDetails = JObject.Parse (blog.validJson);
                blog.DisplayBlogBasicInfo (blog.blogDetails);
                blog.DisplayListOfUrlOfImages (blog.blogDetails, blog);
            }
        }
    }
}