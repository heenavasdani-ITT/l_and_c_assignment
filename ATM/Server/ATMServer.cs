using System;
using System.Net;
using System.Net.Sockets;

// ATMServer acts as a server and start the execution of ATM by establishing connection with client.
public class ATMServer {

    public static void Main(String[] args) {
        StartServer();
        }

    /* startServer is used to connect to a client.
     * It will get Host IP Address that is used to establish a connection
     * In this case, we get one IP address of localhost that is IP : 127.0.0.1 
     * If a host has multiple addresses, you will get a list of addresses
     */
    public static void StartServer() {
        IPHostEntry host = Dns.GetHostEntry("localhost");
        IPAddress ipAddress = host.AddressList[0];
        IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 11000);
        string waitingMsg = "Waiting for a connection...";
        try {
            Socket listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(localEndPoint);
            // Specify how many requests a Socket can listen before it gives Server busy response.  
            // We will listen 10 requests at a time  
            listener.Listen(1);
            Console.WriteLine(waitingMsg);
            // Define another socket for newly created connection
            Socket handler = listener.Accept();
            handler.Shutdown(SocketShutdown.Both);
            handler.Close();
            }
        catch (SocketException e) {
            Console.WriteLine(e.Message);
            }
        }
    }