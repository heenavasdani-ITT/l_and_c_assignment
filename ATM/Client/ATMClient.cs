using System;
using System.Net;
using System.Net.Sockets;

//ATMClient is used to establish connection with the server and proceed as a client.   
public class ATMClient {

    public static void Main(String[] args) {
        StartClient();
        }

    /*startClient is used to connect to a remote server.
     * It will get Host IP Address that is used to establish a connection
     * In this case, we get one IP address of localhost that is IP : 127.0.0.1 
     * If a host has multiple addresses, you will get a list of addresses
     */
    public static void StartClient() {
        string serverErrorMsg = "Unable to connect with server";
        ATM atm = new ATM(20000);
        Card card = new Card();
        if (atm.TakeCardAndPinNumber(card)) {
            IPHostEntry host = Dns.GetHostEntry("localhost");
            IPAddress ipAddress = host.AddressList[0];
            IPEndPoint remoteEndPoint = new IPEndPoint(ipAddress, 11000);
            Socket sender = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            try {
                sender.Connect(remoteEndPoint);
                Console.WriteLine("Socket connected to " + sender.RemoteEndPoint.ToString());
                atm.ExecuteATM();
                }
            catch (SocketException) {
                Console.WriteLine(serverErrorMsg);
                }
            finally {
                //Release the socket
                sender.Shutdown(SocketShutdown.Both);
                sender.Close();
                }
            }
        }
    }