using System;
using System.Text.RegularExpressions;
public class Card {
    public string pinNumber;
    public string cardNumber;
    int maxAttempts = 0;

    public void ValidateCardNumber(string cardNumber) {
        string regexCardNumber = "^4[0-9]{12}(?:[0-9]{3})?$";
        try {
            if (!Regex.IsMatch(cardNumber, regexCardNumber)) {
                throw (new InvalidCardNumberException("Enter Valid Card Number"));
                }
            }
        catch (InvalidCardNumberException e) {
            throw (new ATMException(e.Message));
            }
        }

    public void ValidatePinNumber(Card card) {
        string regexPinNumber = "^[1-9]{1}[0-9]{3}$";
        try {
            if (!Regex.IsMatch(card.pinNumber, regexPinNumber)) {
                throw new InvalidPinNumberException("Enter correct Pin Number");
                }
            }
        catch (InvalidPinNumberException e) {
            Console.WriteLine(e.Message);
            if (++maxAttempts == 3) {
                throw new BlockCardException("Too many invalid pin attempts, so your card is blocked");
                }
            card.pinNumber = Console.ReadLine();
            ValidatePinNumber(card);
            }
        catch (BlockCardException e) {
            throw new ATMException(e.Message);
            }
        }
    }