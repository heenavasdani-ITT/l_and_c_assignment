using System;

// ATM class is used to containe all functionalities related to ATM
public class ATM {
    public static int atmCash;

    public ATM(int balance) {
        atmCash = balance;
        }

    public bool TakeCardAndPinNumber(Card card) {
        string insertCardMsg = "Enter ATM card Number";
        string insertPinMsg = "Enter Pin Number";
        Console.WriteLine(insertCardMsg);
        try {
            card.cardNumber = Console.ReadLine();
            card.ValidateCardNumber(card.cardNumber);
            Console.WriteLine(insertPinMsg);
            card.pinNumber = Console.ReadLine();
            card.ValidatePinNumber(card);
            return true;
            }
        catch (ATMException e) {
            Console.WriteLine(e.Message);
            TakeCardAndPinNumber(card);
            return false;
            }
        catch (FormatException e) {
            Console.WriteLine(e.Message);
            TakeCardAndPinNumber(card);
            return false;
            }
        }

    // It is used to execute some of the operations of ATM machine
    public void ExecuteATM() {
        string confirmExitMsg = "Confirm what do you want:-\n If Want to continue press 1 otherwise press anything\n";
        bool repeatOperation = true;
        try {
            this.SelectOperation(repeatOperation);
            }
        catch (ATMException e) {
            Console.WriteLine(e.Message);
            }
        catch (FormatException e) {
            Console.WriteLine(e.Message);
            }
        finally {
            Console.WriteLine(confirmExitMsg);
            int choice = Int16.Parse(Console.ReadLine());
            if (choice == 1)
                ExecuteATM();
            }
        }

    public void SelectOperation(bool repeatOperation) {
        Account account = new Account();
        string choiceMsg = "Select the operation\n 1. Check Balance 2. Deposit 3. Withdraw 4. Exit\n";
        string choice;
        while (repeatOperation) {
            Console.WriteLine(choiceMsg);
            choice = Console.ReadLine();
            repeatOperation = PerformSelectedOperation(choice, account);
            }
        }

    public bool PerformSelectedOperation(string choice, Account account) {
        string depositMsg = "How much money you want to deposit? Enter the amount\n";
        string withdrawMsg = "How much money you want to withdraw? Enter the amount\n";
        string successfulMsg = "Operation successfully done";
        string exitMsg = "Thankyou for using ATM simulator";
        string invalidChoiceMsg = "Invalid choice, Please select valid choice";
        bool repeatOperation = true;
        int amount;
        switch (choice) {
            case "1":
                Console.WriteLine("Balance is " + account.CheckBalance());
                break;
            case "2":
                Console.WriteLine(depositMsg);
                amount = Int16.Parse(Console.ReadLine());
                ValidateAmountValue(amount);
                account.DepositCash(amount);
                Console.WriteLine(successfulMsg);
                break;
            case "3":
                Console.WriteLine(withdrawMsg);
                amount = Int16.Parse(Console.ReadLine());
                ValidateAmountValue(amount);
                ATM.WithdrawCash(amount);
                account.WithdrawCash(amount);
                Console.WriteLine(successfulMsg);
                break;
            case "4":
                Console.WriteLine(exitMsg);
                repeatOperation = false;
                break;
            default:
                Console.WriteLine(invalidChoiceMsg);
                break;
            }
        return repeatOperation;
        }

    public static void ValidateAmountValue(int amount) {
        try {
            if (amount < 0 || amount == 0) {
                throw (new ArgumentException("Amount cam't be zero or less than zero"));
                }
            }
        catch (ArgumentException e) {
            throw (new ATMException(e.Message));
            }
        }

    public static void WithdrawCash(int amount) {
        try {
            if (amount > atmCash) {
                throw (new InSufficientCashInATMException("Insufficient Cash In ATM"));
                }
            }
        catch (InSufficientCashInATMException e) {
            throw (new ATMException(e.Message));
            }
        }
    }