public class Account {
    public int balance = 10000;

    public void WithdrawCash(int amount) {
        try {
            if (amount > balance) {
                throw (new InSufficientBalanceInAccountException("Insufficient balance in account"));
                }
            balance = balance - amount;
            }
        catch (InSufficientBalanceInAccountException e) {
            throw (new ATMException(e.Message));
            }
        }

    public int CheckBalance() {
        return this.balance;
        }

    public void DepositCash(int amount) {
        balance = balance + amount;
        }
    }