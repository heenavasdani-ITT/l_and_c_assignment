using System;
public class InSufficientCashInATMException : ATMException {

    public InSufficientCashInATMException(string errorMessage) : base(errorMessage) { }
    }