using System;
public class InvalidPinNumberException : ATMException {

    public InvalidPinNumberException(string errorMessage) : base(errorMessage) { }
    }