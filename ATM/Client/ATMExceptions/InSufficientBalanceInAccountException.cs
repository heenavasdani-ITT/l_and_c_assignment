using System;
public class InSufficientBalanceInAccountException : Exception {

    public InSufficientBalanceInAccountException(string errorMessage) : base(errorMessage) { }
    }