using System;
public class BlockCardException : InvalidCardNumberException {
    public BlockCardException(string errorMessage) : base(errorMessage) { }
    }