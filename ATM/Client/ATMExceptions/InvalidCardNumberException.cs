using System;
public class InvalidCardNumberException : Exception {

    public InvalidCardNumberException(string errorMessage) : base(errorMessage) { }
    }