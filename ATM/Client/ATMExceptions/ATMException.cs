using System;
public class ATMException : Exception {

    public ATMException(string errorMessage) : base(errorMessage) { }
    }