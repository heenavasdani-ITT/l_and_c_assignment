/*
* Question:-
* Look at the below classes and the client code given below on how the object are used and methods invoked. 
* Is there a better way to write the Customer class?
*/

public class Customer {
    private String firstName;
    private String lastName;
    private Wallet myWallet;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Wallet getWallet() {
        return myWallet;
    }
}

public class Wallet {
    private float value;

    public float getTotalMoney() {
        return value;
    }

    public void setTotalMoney(float newValue) {
        value = newValue;
    }

    public void addMoney(float deposit) {
        value += deposit;
    }

    public void subtractMoney(float debit) {
        value -= debit;
    }
}

    // Client code…. assuming some delivery boy wants to get his payment

    // code from some method inside the delivery boy class...
    payment=2.00; // “I want my two dollars!”

    Wallet theWallet = myCustomer.getWallet();
    if(theWallet.getTotalMoney()>payment)
    {
        theWallet.subtractMoney(payment);
    }
    else
    {
        // come back later and get my money
    }

    /*
     * Solution :
     * 
     * Why is the above code not good?
     * 
     * There are few points through which it will be clear that "why the above code is not good":-
     * 
     * (1) The delivery boy is being exposed to more information than he needs to be. 
     * Like he only wants the payment for his service,
     * he does not need to know how much money a client has in his wallet.
     * It is called Law of Demeter violation.
     * 
     * (2) From the above code, the delivery boy class knows that the customer has a wallet. 
     * So, through this information, he can manipulate the value of money in wallet.
     * It is called Single Responsibility Principle violation.
     * 
     * (3) It may be happen that the customer's wallet has been stolen by delivery boy 
     * or I can say, it will be set null in the value of wallet.
     */