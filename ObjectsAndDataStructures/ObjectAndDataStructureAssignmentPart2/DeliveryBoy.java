// The code inside the delivery boy class can be modified as below:-
public class DeliveryBoy {
    private static double payment = 2.00; 

    public static void main(String[] args) {
        Customer myCustomer = new Customer();
        myCustomer.payPayment(payment);
    }
}
/*
 * Why is the above modified code is good?
 * 
 * There are few points through which it will be clear that "why is the above modified code is good":-
 * 1) The delivery boy does not have direct access to the wallet.
 * Here delivery boy doesn't know that the money comes from a wallet, 
 * 
 * 2) The Wallet class can now change and now the delivery boy class will not affect from the changes inside wallet class. 
 * the LoD is also known as the principle of least knowledge.
 * The relationship between the delivery boy class and the Wallet class has been removed, the code is less coupled.
 * 
 * 3) The delivery boy is not being exposed to more information than he needs to be. 
 * 
 * 4) The delivery boy will not need to face the NullPointerException.
 */