//The following is the better way to write the customer class:-
public class Customer {
    private String firstName;
    private String lastName;
    private Wallet myWallet = new Wallet();

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void payPayment(double payment) {
        String successfulMessage = "Payment is successfully done";
        String errorMessage = "Payment is not successful, Please come back later and get money!";
        try {
            if (myWallet.getTotalMoney() < payment) {
                throw new InSufficientBalanceException(errorMessage);
            }
            myWallet.subtractMoney(payment);
            System.out.println(successfulMessage);
        } catch (InSufficientBalanceException e) {
            System.out.println(e.getMessage());
        }
    }
}