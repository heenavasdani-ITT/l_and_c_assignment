public class InSufficientBalanceException extends Exception {

    private String exceptionMessage;

    public InSufficientBalanceException(String message) {
        exceptionMessage = message;
    }

    public String getMessage() {
        return exceptionMessage;
    }

}