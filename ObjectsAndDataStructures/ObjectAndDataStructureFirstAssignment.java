// Question:-

Class Employee { 
  string name;
  int age; 
  float salary; 
  public : string getName(); 
  void setName(string name); 
  int getAge(); 
  void setAge(int age); 
  float getSalary();
  void setSalary(float salary);
}; 
Employee employee; 

//Is 'employee' an object or a data structure? Why ?

/*
Answer :-

employee is a data structure.
There are few points through which it will be clear:- 

1)Employee class implemented to hold only the data and methods 
like getter and setter which is related to an employee's data.

2)The methods of this class basically provide a functionality 
for accessing the data related to an employee by creating getter and setter, instead of any real significant work.

3)The methods like getAge(),getSalary(),getName(),setName(),setAge() and setSalary() 
don't change the purpose and behaviour of Employee class.

So, employee is a data structure because "Employee" class, exposes its data (variables) 
and have no meaningful (significant) methods.
*/