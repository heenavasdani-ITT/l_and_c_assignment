﻿using System;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace API {
    ///<summary>Class<c>Tumblr</c>represents Tumblr
    ///which is an American microblogging and social networking website.
    /// The service allows users to post multimedia
    ///other content to a short-form blog.</summary>
    public class Tumblr {
        public string blogName, startPostNum, endPostNum,
            blogUrl, postRange, validJson;
        public int postNumber;

        /// <summary>method<c>GetResponse</c>is used to 
        ///fetch the response by using the url of respective blog.</summary>
        public void GetTumblrApiResponse(Tumblr tumblr) {
            HttpWebRequest request = tumblr.GetRequest(tumblr.blogUrl);
            var httpResponse = (HttpWebResponse)request.GetResponse();
            tumblr.ConvertResponseToValidJson(httpResponse, tumblr);
            }

        /// <summary>method<c>GetRequest</c>is used to 
        ///create HttpWebRequest with parameters.</summary>
        public HttpWebRequest GetRequest(string blogUrl) {
            var request = (HttpWebRequest)WebRequest.Create(blogUrl);
            return request;
            }

        ///<summary>method<c>ConvertResponseToValidJson</c>is used to convert 
        ///invalid json to valid json in response.</summary>
        public void ConvertResponseToValidJson(HttpWebResponse httpResponse, Tumblr tumblr) {
            using(var response = new StreamReader(httpResponse.GetResponseStream())) {
                tumblr.validJson = response.ReadToEnd();
                tumblr.validJson = tumblr.validJson.Substring(tumblr.validJson.IndexOf("{"));
                tumblr.validJson = tumblr.validJson.Remove(tumblr.validJson.LastIndexOf(";"));
                httpResponse.GetResponseStream().Close();
                }
            }

        /// <summary>method <c>GetStartAndEndPostNum</c>is used to find
        ///starting and ending post number from given post range.</summary>
        public void GetStartAndEndPostNum(Tumblr tumblr) {
            int rangeSeparatorIndex = 0;
            for(int i = 0; i < tumblr.postRange.Length; i++) {
                if(Char.IsDigit(tumblr.postRange[i])) {
                    rangeSeparatorIndex = ++i;
                    break;
                    }
                }
            tumblr.startPostNum = tumblr.postRange.Substring(0, rangeSeparatorIndex);
            tumblr.endPostNum = tumblr.postRange.Substring(rangeSeparatorIndex + 1);
            }

        ///<summary>method<c>CheckValidityOfStartAndEndPostNum</c>is used to 
        ///check validity of starting and ending post number.</summary>
        public bool CheckValidityOfStartAndEndPostNum(string startPostNum, string endPostNum) {
            int startPostNumInInteger = Int32.Parse(startPostNum);
            int endPostNumInInteger = Int32.Parse(endPostNum);
            if(startPostNumInInteger < 0 || endPostNumInInteger < 0) {
                return false;
                }
            else if(startPostNumInInteger > endPostNumInInteger) {
                return false;
                }
            else
                return true;
            }

        ///<summary>method<c>GetBlogUrl</c>is used to get Blog url.</summary>
        public void GetBlogUrl(Tumblr tumblr) {
            string noOfPost =
                (Int32.Parse(tumblr.endPostNum) - Int32.Parse(tumblr.startPostNum) + 1)
                .ToString();
            tumblr.blogUrl = @"https://" + tumblr.blogName +
                ".tumblr.com/api/read/json?type=photo&num=" + noOfPost +
                "&start=" + (Int32.Parse(tumblr.startPostNum) - 1).ToString();
            }

        ///<summary>method<c>IsBlogNameValid</c>is used to check validity of Blog Name.</summary>
        public bool IsBlogNameValid(string blogName) {
            var patternForChar = new Regex("^[a-zA-Z ]+$");
            if(patternForChar.IsMatch(blogName))
                return true;
            else
                return false;
            }
        }
    }
