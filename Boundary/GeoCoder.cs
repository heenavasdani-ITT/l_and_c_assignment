﻿using System;
using RestSharp;

namespace Boundary {
    /// <summary>Class <c>GeoCoder</c>used to create request for geocoding and display repsonse of that request.</summary>
    class GeoCoder {

        static void Main(string[] args) {
            Console.WriteLine("**********************Enter the place name/postal code which you want to geocode**********************\n");
            string placeNameOrPostalCode = Console.ReadLine();
            GeocodingAPI geoCodingAPI = new GeocodingAPI();
            RestRequest request = geoCodingAPI.CreateRequest(placeNameOrPostalCode);
            try {
                Location location = geoCodingAPI.GetResponse(request);
                Console.WriteLine("Latitude = " + location.Lat);
                Console.WriteLine("Longitude = " + location.Lon);
                }
            catch (APIResponseException e) {
                Console.WriteLine(e.Message);
                }
            }
        }
    }