using System;
using NUnit.Framework;
using RestSharp;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Boundary.Tests {

    [TestFixture]
    public class GeocodingAPITests {
        RestClient restClient = new RestClient(AppConfig.RequestUrl);
        RestRequest request = new RestRequest(AppConfig.EndPoint, RestSharp.Method.GET);

        [SetUp]
        public void Setup() {
            request.AddParameter("key", AppConfig.Key);
            request.AddParameter("format", "json");
            }

        [TestCase("Mysuru", 76.6553609, 12.3051828)]
        [TestCase("302012", 75.5261444, 26.9792916)]
        public void Get_ValidRequest_ReturnsCorrectResponse(string placeNameOrPostalCode, double longitude, double latitude) {
            request.AddParameter("q", placeNameOrPostalCode);
            var response = restClient.Execute(request);
            List<Location> location = JsonConvert.DeserializeObject<List<Location>>(response.Content);
            Assert.AreEqual(longitude, Math.Round(location[0].Lon, 7), "The value of longitude shoulde be " + longitude);
            Assert.AreEqual(latitude, Math.Round(location[0].Lat, 7), "The value of latitude shoulde be " + latitude);
            }

        [TestCase("Mysuru")]
        public void Get_ValidRequest_ReturnsOKStatusCode(string placeNameOrPostalCode) {
            request.AddParameter("q", placeNameOrPostalCode);
            var response = restClient.Execute(request);
            Assert.AreEqual(200, (int)response.StatusCode, "Request is not processed successfully");
            }

        [TestCase("")]
        [TestCase(" ")]
        public void Get_EmptyAddressInRequest_ReturnsBadRequestStatusCode(string placeNameOrPostalCode) {
            request.AddParameter("q", placeNameOrPostalCode);
            var response = restClient.Execute(request);
            Assert.AreEqual(400, (int)response.StatusCode, "Bad Request Passed");
            }

        [TestCase("@")]
        [TestCase(".!")]
        [TestCase("111ee11")]
        public void Get_InvalidAddressInRequest_ReturnsNotFoundStatusCode(string placeNameOrPostalCode) {
            request.AddParameter("q", placeNameOrPostalCode);
            var response = restClient.Execute(request);
            Assert.AreEqual(404, (int)response.StatusCode, "Invalid address Passed.");
            }

        [TestCase("", "Invalid Request")]
        [TestCase(".!", "Unable to geocode")]
        [TestCase("Mysurur", "Unable to geocode")]
        public void Get_InvalidRequest_ReturnsErrorMsg(string placeNameOrPostalCode, string expectedErrorMsg) {
            request.AddParameter("q", placeNameOrPostalCode);
            var response = restClient.Execute(request);
            GeocodingAPIError responseContent = JsonConvert.DeserializeObject<GeocodingAPIError>(response.Content);
            Assert.AreEqual(expectedErrorMsg, responseContent.error, "Correct Error Message is not present in response.");
            }

        [TestCase("Mysuru", "5b970b99795de")]
        public void Get_InvalidAPIKey_ReturnsCorrectStatusCodeAndErrorMsg(string placeNameOrPostalCode, string invalidKey) {
            request.AddParameter("q", placeNameOrPostalCode);
            request.AddParameter("key", invalidKey);
            var response = restClient.Execute(request);
            GeocodingAPIError responseContent = JsonConvert.DeserializeObject<GeocodingAPIError>(response.Content);
            Assert.AreEqual(401, (int)response.StatusCode, "Unauthorized Request Passed");
            Assert.AreEqual("Invalid key", responseContent.error, "Correct Error Message is not present in response");
            }

        [TearDown]
        public void TearDown() {
            restClient.Delete(request);
            }
        }
    }