namespace Boundary {
    /// <summary>Class <c> GeocodingAPIError </c>contains field for representing content of response for invalid Request.</summary>
    class GeocodingAPIError {
        public string error {
            get;
            set;
            }
        }
    }