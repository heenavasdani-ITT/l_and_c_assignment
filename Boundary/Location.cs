using System.Collections.Generic;

namespace Boundary {
    /// <summary>Class <c> Location </c>contains all fields which appears in response after passing any geocoding request for any place.</summary>
    class Location {
        public string PlaceId {
            get;
            set;
            }
        public string Licence {
            get;
            set;
            }
        public string OsmType {
            get;
            set;
            }
        public string OsmId {
            get;
            set;
            }
        public List<string> BoundingBox {
            get;
            set;
            }
        public double Lat {
            get;
            set;
            }
        public double Lon {
            get;
            set;
            }
        public string DisplayName {
            get;
            set;
            }
        public string Class {
            get;
            set;
            }
        public string Type {
            get;
            set;
            }
        public string Importance {
            get;
            set;
            }
        public string Icon {
            get;
            set;
            }
        }
    }