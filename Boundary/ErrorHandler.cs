using System;
using RestSharp;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Boundary {
    /// <summary>Class <c>ErrorHandler</c>used to handle all type of Exceptions which can be generate during getting response.</summary>
    class ErrorHandler {
        private void CheckStatusCode(IRestResponse response) {
            string notFoundMsg = "Place Name is not Exist", badRequestMsg = "Query is missing from request",
                   unauthorizedMsg = "Request denied because of unauthorization",
                   internalServerMsg = "Error: getaddrinfo ENOTFOUND";

            if ((int)response.StatusCode == 404) {
                Console.WriteLine((int)response.StatusCode);
                throw new NotFoundException(notFoundMsg);
                }
            else if ((int)response.StatusCode == 400) {
                throw new BadRequestException(badRequestMsg);
                }
            else if ((int)response.StatusCode == 401) {
                throw new UnauthorizeException(unauthorizedMsg);
                }
            else if ((int)response.StatusCode == 500) {
                throw new InternalServerException(internalServerMsg);
                }
            else {
                Console.WriteLine("\n**********************Response**********************\n");
                }
            }

        internal Location GetResponse(IRestResponse response) {
            try {
                CheckStatusCode(response);
                List<Location> location = JsonConvert.DeserializeObject<List<Location>>(response.Content);
                return location[0];
                }
            catch (NotFoundException e) {
                throw new APIResponseException(e.Message);
                }
            catch (BadRequestException e) {
                throw new APIResponseException(e.Message);
                }
            catch (UnauthorizeException e) {
                throw new APIResponseException(e.Message);
                }
            catch (InternalServerException e) {
                throw new APIResponseException(e.Message);
                }
            }
        }
    }