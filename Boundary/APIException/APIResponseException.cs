using System;

///<summary>Class <c>APIResponseException</c>is the base class of all other type of Exception which can be generate after sending any API request.</summary>
public class APIResponseException : Exception {
    public APIResponseException(string errorMessage) : base(errorMessage) { }
    }