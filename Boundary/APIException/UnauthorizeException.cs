public class UnauthorizeException : APIResponseException {
    public UnauthorizeException(string errorMessage) : base(errorMessage) { }
    }