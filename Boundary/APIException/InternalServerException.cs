public class InternalServerException : APIResponseException {
    public InternalServerException(string errorMessage) : base(errorMessage) { }
    }