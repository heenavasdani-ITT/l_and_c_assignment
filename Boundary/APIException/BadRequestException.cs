public class BadRequestException : APIResponseException {
    public BadRequestException(string errorMessage) : base(errorMessage) { }
    }