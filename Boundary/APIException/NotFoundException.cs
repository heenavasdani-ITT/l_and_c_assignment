public class NotFoundException : APIResponseException {
    public NotFoundException(string errorMessage) : base(errorMessage) { }
    }