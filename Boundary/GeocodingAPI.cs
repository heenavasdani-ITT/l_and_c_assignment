using RestSharp;

namespace Boundary {
    /// <summary>Class <c>GeoCodingAPI</c>used to create request for geocoding and give repsonse of that request.</summary>
    class GeocodingAPI {
        RestClient restClient = new RestClient(AppConfig.RequestUrl);
        ErrorHandler errorHandler = new ErrorHandler();
        internal RestRequest CreateRequest(string placeNameOrPostalCode) {
            RestRequest request = new RestRequest(AppConfig.EndPoint, RestSharp.Method.GET);
            request.AddParameter("key", AppConfig.Key);
            request.AddParameter("q", placeNameOrPostalCode);
            request.AddParameter("format", "json");
            return request;
            }

        internal Location GetResponse(RestRequest request) {
            var response = restClient.Execute(request);
            return errorHandler.GetResponse(response);
            }
        }
    }